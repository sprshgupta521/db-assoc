/* eslint-disable prettier/prettier */
import { ApiProperty } from '@nestjs/swagger';

import { BelongsToMany, Table,Model, Column, ForeignKey,BelongsTo, HasMany} from 'sequelize-typescript';

@Table 
export class User extends Model<User> {
  destroy: any;

@Column
firstName: string;

@Column
lastName: string;
  save: any;

@HasMany(()=> Cat)
cat: Cat[]

@ForeignKey(()=> Cat)
@Column
catId: number;

@ForeignKey(()=> IsLive)
@Column
isId: number;
}
// one to many

@Table 
export class Cat extends Model {
  /**
   * The name of the Cat
   * @example Kitty
    **/
  @Column
  name: string;

  @Column
  num: number;

  @ForeignKey(() => User)
  @Column
   catId : number;

  @BelongsToMany(()=> User, ()=> IsLive)
   isLive: IsLive[]


  @ApiProperty({ example: 1, description: 'The age of the Cat' })
  age: number;

  @ApiProperty({
    example: 'Maine Coon',
    description: 'The breed of the Cat',
  })
  breed: string;
}


@Table 
export class IsLive extends Model{
  
  @Column
  addressId: number
   @ForeignKey(()=>User)
   user: User[]

  
  
  @ForeignKey(()=>Cat)
  cat: Cat[]
  @Column
  catId: number
  
}
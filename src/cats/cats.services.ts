/* eslint-disable prettier/prettier */
import { Injectable } from '@nestjs/common';
import { CreateCatDto } from './dto/create-cat.dto';
import {CreateCatsDto} from './dto/create-cats.dto';
import { User,Cat } from './entities/cats.entity';
import { InjectModel } from '@nestjs/sequelize';



@Injectable()
export class UserService {
  
  constructor(
    @InjectModel(User)
    private readonly userModel: typeof User,
   ) {}

 async create(CreateCatDto: CreateCatDto): Promise<User> {
    const user = new User();
    user.firstName = CreateCatDto.firstname;
    user.lastName = CreateCatDto.lastname;

    return user.save();
  }

  async findAll(): Promise<User[]> {
    return this.userModel.findAll<User>();
  }

 
  async findOne(id: string): Promise<User> {
    
    return this.userModel.findOne({
      where: {
        id,
      },
    });
  }

  async remove(id: string): Promise<void> {
    const user = await this.findOne(id);
    await user.destroy();
  }
}



export class CatService {
  
  constructor(
    @InjectModel(Cat)
    private readonly userModel: typeof Cat,
   ) {}

 async create(CreateCatsDto: CreateCatsDto): Promise<Cat> {
    const user = new Cat();
    user.name = CreateCatsDto.name;
    user.num = CreateCatsDto.num;

    return user.save();
  }

  async findAll(): Promise<Cat[]> {
    return this.userModel.findAll<Cat>();
  }

 
  async findOne(id: string): Promise<Cat> {
    
    return this.userModel.findOne({
      where: {
        id,
      },
    });
  }

  async remove(id: string): Promise<void> {
    const cat = await this.findOne(id);
    await cat.destroy();
  }
}
  

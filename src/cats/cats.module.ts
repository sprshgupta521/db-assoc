/* eslint-disable prettier/prettier */
import { Module } from '@nestjs/common';
import { CatsController,Cats } from './cats.controller';
import { CatService, UserService } from './cats.services';
import { SequelizeModule } from '@nestjs/sequelize';
import { User,Cat,IsLive } from './entities/cats.entity';



@Module({
  imports: [SequelizeModule.forFeature([User,Cat,IsLive])],
  controllers: [CatsController,Cats],
  providers: [UserService,CatService],
})
export class CatsModule {}
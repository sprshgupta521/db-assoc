/* eslint-disable prettier/prettier */
import { IsInt, IsString } from 'class-validator';
// eslint-disable-next-line prettier/prettier

export class CreateCatDto {
  @IsString()
  readonly firstname: string;

  @IsString() 
  readonly lastname: string;
}
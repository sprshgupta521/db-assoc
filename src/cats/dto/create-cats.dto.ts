import { IsInt, IsString } from 'class-validator';
// eslint-disable-next-line prettier/prettier

export class CreateCatsDto {
  @IsString()
  readonly name: string;

  @IsInt() 
  readonly num: number;
}
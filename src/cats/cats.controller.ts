/* eslint-disable prettier/prettier */
import { Body, Controller, Get, Param, Post,Delete ,Res,HttpStatus} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { UserService, CatService } from './cats.services';
import { CreateCatDto } from './dto/create-cat.dto';
import {CreateCatsDto} from './dto/create-cats.dto';

import { Cat, User } from './entities/cats.entity';

@ApiBearerAuth()
@ApiTags('User')
@Controller('users')
export class CatsController {
  constructor(private readonly userService: UserService) {}

  @Post()
  @ApiOperation({ summary: 'Create cat' })
  async create(@Body() createCatDto: CreateCatDto): Promise<User> {
    return this.userService.create(createCatDto);
  }

  @Get()
  @ApiResponse({
    status: 200,
    description: 'The found record',
    type: User,
  })
  findAll(): Promise<User[]>{
    return this.userService.findAll();
  }


  @Get(':id')
  public async findOne(@Param('id') id: string,@Res() res): Promise<User> {
    if (!id) throw  ("Error");
    return this.userService.findOne(id);
  
  }

  @Delete(':id')
  remove(@Param('id') id: string): Promise<void> {
    if (!id) throw  ("Error");
    return this.userService.remove(id);
  }
}

@ApiBearerAuth()
@ApiTags('Cat')
@Controller('cats')
export class Cats {
  constructor(private readonly CatService: CatService) {}

  @Post()
  @ApiOperation({ summary: 'Create cat' })
  async create(@Body() createCatsDto: CreateCatsDto): Promise<Cat> {
    return this.CatService.create(createCatsDto);
  }

  @Get()
  @ApiResponse({
    status: 200,
    description: 'The found record',
    type: User,
  })
  findAll(): Promise<Cat[]>{
    return this.CatService.findAll();
  }


  @Get(':id')
  public async findOne(@Param('id') id: string,@Res() res): Promise<User> {
    if (!id) throw  ("Error");
    const user = await this.CatService.findOne(id);
        return res.status(HttpStatus.OK).json(user);;
  
  }

  @Delete(':id')
 public async remove(@Param('id') id: string, @Res() res): Promise<void> {
    if (!id) throw  ("Error");
    const user = await this.CatService.remove(id);
        return res.status(HttpStatus.OK).json(user);;
  }
}